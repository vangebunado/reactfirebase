import { RouterProvider, createBrowserRouter } from "react-router-dom";
import { Root } from "./root";
import Login from "../pages/login";
import Landing from "../pages/landing";
import SignUp from "../pages/signup";
import Dashboard from "../pages/Dashboard";
import Voucher from "../pages/Voucher";
import Announcement from "../pages/Announcement";
import RequestForm from "../pages/RequestForm";
import AvailableVoucher from "../pages/AvailableVoucher";


const AppRouters = () => {
  const router = createBrowserRouter([
    {
      path: '/',
      element: <Root />,
      children: [
        {
          path: '/',
          element: <SignUp/>,
        },
        {
          path: '/Landing',
          element: <Landing />,
        },
        {
          path: '/login',
          element: <Login />,
        },
        {
          path: '/Dashboard',
          element: <Dashboard/>,
        },
        {
          path: '/Voucher',
          element: <Voucher/>,
        },
        {
          path: '/AvailableVoucher',
          element: <AvailableVoucher/>,
        },
        {
          path: '/Announcement',
          element: <Announcement/>,
        },
        {
          path: '/RequestForm',
          element: <RequestForm/>,
        },
      ],
    },
  ]);

  return <RouterProvider router={router} />;
};

export default AppRouters;