import AppRouters from "./components/app-router";
import { ColorModeContext, useMode } from "./components/theme";
import {CssBaseline, ThemeProvider } from "@mui/material";
function App() {
  const [theme, colorMode] = useMode();

  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline/>
            <AppRouters/>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
}

export default App;
