import React, { useState } from 'react';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Stack from '@mui/material/Stack';
import { styled } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { AppBarComponent } from './components/appbar';
import { Button, Container, Typography } from '@mui/material';
import { RequestFormModal } from './components/request-modal';

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }));

const Dates = styled(Paper)(({ theme }) => ({
    boxShadow: 'none',
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

const Announcement = () => {
    const defaultTheme = createTheme();

    const [open, setOpen] = useState(false); // State for modal open

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
  return (
    <ThemeProvider theme={defaultTheme}>
        <CssBaseline/>
        <AppBarComponent/>

        <Box sx={{ width: '100%', marginTop: '0.2%', marginBottom: '0.2%', padding: '50px'}}>
            <Stack spacing={2}>
                <Typography variant='h3' sx={{textAlign: 'center'}}>
                    Attention to all students
                </Typography>
                <Item>
                    <Container style={{marginBottom: '20px', marginTop: '20px'}}>
                        <Typography variant='h6'>
                            MIS OFFICE IS NOW ON A SEMENAR THERE IS NO AVAILABLE VOUCHER ON THIS DATES:
                        </Typography>
                    </Container>

                    <Stack spacing={1}>
                        <Dates>12/12/12</Dates>
                        <Dates>12/12/12</Dates>
                        <Dates>12/12/12</Dates>
                        <Dates>12/12/12</Dates>
                        <Dates>12/12/12</Dates>
                    </Stack>

                    {/* Comment this area to remove the post button */}
                    <Button variant='filled' sx={{marginTop: '10px', bgcolor: '#a9f562', width: '10%', marginBottom: '10px', ':hover':{color: '#ffffff', bgcolor: '#74b538'}}} onClick={handleOpen}>Post</Button>
                </Item>

                <RequestFormModal open={open} handleClose={handleClose}/>
            </Stack>
        </Box>

    </ThemeProvider>
  )
}
export default Announcement;