import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Button from '@mui/material/Button';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';

import Logo from '../../images/logo.svg'

export const AppBarComponent = () => {
  return (
    <>
        <AppBar
            position="static"
            color="default"
            elevation={0}
            sx={{ borderBottom: (theme) => `1px solid ${theme.palette.divider}`, bgcolor: '#c1f68f' }}
        >
            <Toolbar sx={{ flexWrap: 'wrap' }}>
            <img src={Logo} alt="" style={{ width: '3%', paddingRight: '5px' }} />
            <Typography variant="h6" color="inherit" noWrap sx={{ flexGrow: 1 }}>
                MIS 
            </Typography>
            <nav>
                <Link
                color="text.primary"
                href="#"
                sx={{ my: 1, mx: 1.5, textDecoration: 'none' }}
                >
                Dashboard
                </Link>
                <Link
                variant="button"
                color="text.primary"
                href="#"
                sx={{ my: 1, mx: 1.5, textDecoration: 'none' }}
                >
                Available Voucher
                </Link>
                <Link
                variant="button"
                color="text.primary"
                href="#"
                sx={{ my: 1, mx: 1.5, textDecoration: 'none' }}
                >
                Analytics
                </Link>
                <Link
                variant="button"
                color="text.primary"
                href="/announcement"
                sx={{ my: 1, mx: 1.5, textDecoration: 'none' }}
                >
                Announcement
                </Link>
            </nav>
            <Button href="#" variant="filled" sx={{ my: 1, mx: 1.5, bgcolor: '#72a641', color: '#ffffff' }}>
                Logout
            </Button>
            </Toolbar>
        </AppBar>
    </>
  )
}
