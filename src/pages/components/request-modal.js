import React, { useState} from 'react';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';
import { Stack } from '@mui/material';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '30%',
  bgcolor: 'background.paper',
  boxShadow: 24,
  item: 'center',
  paddingTop: '20px',
  p: 4,
};

export const RequestFormModal = ({ open, handleClose}) => {
  const [name, setName] = useState('');
  const [department, setDepartment] = useState('');
  

  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
      style={{ margin: 0 }}
    >
      <Box sx={style}>

        <Typography id="modal-modal-description" sx={{ mt: 2, fontWeight: 'bold', mb: 5 }}>
          Request Form
        </Typography>

        <Stack spacing={2}>
            <TextField
            id="outlined-select-currency"
            autoFocus
            label="Name"
            value={name}
            onChange={(event) => setName(event.target.value)}
            />
            <TextField
            id="outlined-select-currency"
            label="Department"
            value={department}
            onChange={(event) => setDepartment(event.target.value)}
            />
        </Stack>
        <Container style={{ width: '100%', marginTop: '20px', alignItems: 'center' }}>
          <Button variant="contained" fullWidth sx={{ mt: 3, mb: 2, bgcolor: '#a9f562', color: '#000000', textTransform: 'none', ':hover':{color: '#ffffff', bgcolor: '#74b538'}}} >
            Submit Incident
          </Button>
        </Container>
      </Box>
    </Modal>
  );
};
