import React from 'react';
import Box from '@mui/material/Box';
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import DashboardIcon from '@mui/icons-material/Dashboard';
import BarChartIcon from '@mui/icons-material/BarChart';
import InsertDriveFileIcon from '@mui/icons-material/InsertDriveFile';
import LocalActivityIcon from '@mui/icons-material/LocalActivity';
import EventAvailableIcon from '@mui/icons-material/EventAvailable';
import AddAlertIcon from '@mui/icons-material/AddAlert';
import LogoutIcon from '@mui/icons-material/Logout';
import Dashboard from './Dashboard';

const Landing = () => {
  const [value, setValue] = React.useState(0);
  return (

  
    <div >
      
<Box sx={{ minWidth: 100 }}>
<BottomNavigation
  showLabels
  value={value}
  onChange={(_event, newValue) => {
    setValue(newValue);
    
  }}
>
      <BottomNavigationAction label="Dashboard" value="/Dashboard" to="/Dashboard" href='/Dashboard' icon={<DashboardIcon/>} />
      <BottomNavigationAction label="Request Form" href='/RequestForm' icon={<InsertDriveFileIcon/>} />
      <BottomNavigationAction label="Voucher" href='/Voucher'icon={<LocalActivityIcon/>} />
      <BottomNavigationAction label="Availble Voucher" href='/AvailableVoucher' icon={<EventAvailableIcon />} />
      <BottomNavigationAction label="Announcement" href='/Announcement' icon={<AddAlertIcon/>} />
      <BottomNavigationAction label="Analytics" href='/Analytics' icon={<BarChartIcon/>} />
      <BottomNavigationAction label="Logout" href='/' icon={<LogoutIcon/>} />
    </BottomNavigation>

  </Box>
  <Dashboard/>
</div>
  )
};
export default Landing;