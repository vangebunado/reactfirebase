// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
export const firebaseConfig = {
  apiKey: "AIzaSyAkwr_etnOgQ1RFGH0tCORwAVt356h-en0",
  authDomain: "sisterreact-9556d.firebaseapp.com",
  projectId: "sisterreact-9556d",
  storageBucket: "sisterreact-9556d.appspot.com",
  messagingSenderId: "375585721273",
  appId: "1:375585721273:web:6eef41eea2b2ceadaa0468",
  measurementId: "G-585MSENK11"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);

